
//! This repo demonstrates a rust implementation of multi-agent pathfinding
//! algorithm based on David Silver's 2005 paper "Cooperative Pathfinding".
//! 
//! The code presented here did not fully cover proposed solution of that paper,
//!  only partial of it.
//! 
//! For more information, please read the paper "Cooperative Pathfinfing" 
//! by David Silver. (http://www.aaai.org/Library/AIIDE/2005/aiide05-020.php)
//! 
 
use indexmap::IndexMap;
use indexmap::map::Entry::{Occupied, Vacant};
use num_traits::{Zero};
use std::usize;
use std::hash::Hash;
use std::collections::{BinaryHeap, HashMap};
use std::cmp::{Ordering, min, max};
use itertools;

type AgentId        = isize;
type TimeStep       = usize;
type Constraint<N>  = (AgentId, N, TimeStep);
type Constraints<N> = Vec<Constraint<N>>;


struct CTNode<N> {
    constraints: Constraints<N>,
    solution:    HashMap<isize, Vec<N>>,
}

impl<N: Eq+Clone+Hash> CTNode<N> {
    fn new( cons: Constraints<N>, solu: HashMap<isize, Vec<N>>) -> Self {
        CTNode {
            constraints: cons,
            solution:   solu,
        }
    }

    fn get_sic(&self) -> usize {
        let mut cost = 0;
        for (_, path) in &self.solution {
            cost += path.len();
        }

        cost
    }

    /// conflict type
    /// map: 
    ///            A
    ///         B  o  D
    ///            C
    /// two conflicts:
    /// 1. agent1 (B->o), agent2 (D->o) || agent1 (B->o), agent2 (A->o)
    /// 2. agent1 (B->o), agent2 (o->B)
    fn get_first_conflict(&self) -> Option<Constraints<N>> {
        let max_timestep = self.solution.iter()
                                        .max_by_key(|(_, x)| x.len())?
                                        .1
                                        .len();

        for t in 0..max_timestep {
            let mut spacetime_map = HashMap::new();

            // condition 1
            for (agent, path) in &self.solution {
                let curr_i = min(t, path.len()-1);
                let prev_i = min(max(t-1, 0), path.len()-1);
                let opt = spacetime_map.insert(path[curr_i].clone(), 
                                               (*agent, path[prev_i].clone()));
                if let Some((prev_agent, _)) = opt {
                    return Some (vec![
                            (*agent, path[curr_i].clone(), t), 
                            (prev_agent, path[curr_i].clone(), t)
                    ]);
                }
            }

            // condition 2
            if t != 0 {
                for (cur1, (agent1, prev1)) in &spacetime_map {
                    if let Some((agent2, prev2)) = spacetime_map.get(prev1) {
                        if cur1 == prev2 {
                            return Some(vec![
                                    (*agent1, cur1.clone(), t),
                                    (*agent2, prev1.clone(), t)
                            ]);
                        }
                    }
                }
            }
        }
        None
    }
}

impl<N: Eq+Clone+Hash> PartialEq for CTNode<N> {
    fn eq(&self, other: &CTNode<N>) -> bool {
        self.get_sic().eq(&other.get_sic())
    }
}

impl<N: Eq+Clone+Hash> Eq for CTNode<N> {}

impl<N: Eq+Clone+Hash> PartialOrd for CTNode<N> {
    fn partial_cmp(&self, other: &CTNode<N>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<N: Eq+Clone+Hash> Ord for CTNode<N> {
    fn cmp(&self, other: &CTNode<N>) -> Ordering {
        self.get_sic().cmp(&other.get_sic())
    }
}

///
pub fn cbs<N, C, FN, IN, FH> (
    agents: &Vec<(isize, N, N)>,
    neighbours: &FN,
    heuristic:  &FH,
) -> Option<HashMap<isize, Vec<N>>>
where
    N: Eq + Hash + Clone,
    C: Zero + Ord + Copy,
    FN: Fn(&N) -> IN,
    IN: IntoIterator<Item = (N, C)>,
    FH: Fn(&N) -> C,
{
    let mut solutions     = HashMap::with_capacity(agents.len());
    let mut agent_helpers = HashMap::with_capacity(agents.len());

    for (id, start, goal) in agents {
        let mut agent_helper = Agent::new(start, goal, neighbours, heuristic);
        let solu = agent_helper.find_solution()?;
        solutions.insert(*id, solu);
        agent_helpers.insert(*id, agent_helper);
    }

    // CBS
    let mut open = BinaryHeap::new();
    open.push(CTNode::new(Vec::<Constraint<N>>::new(),
                                   solutions.clone()));

    while let Some(min_cost) = open.pop() {
        match min_cost.get_first_conflict() {
            // fix the conflict of agent 1 and agent 2 at time t
            Some(all_cons) => {
                for (id, vertex, t) in &all_cons {
                    let mut cons = min_cost.constraints.clone();
                    cons.push((*id, vertex.clone(), *t));
                    let mut solu = min_cost.solution.clone();
                    // update solutions by constraints
                    let updated; // the result of update solutions
                    {
                        let cons = cons.iter().filter(|(p1, _, _)| {
                            return p1 == id;
                        }).cloned().collect::<Constraints<N>>();
                        let agent_helper = agent_helpers.get_mut(id)?;
                        let agent_path   = solu.get_mut(id)?;
                        updated = agent_helper.update_solution(&cons, agent_path);
                    }

                    if updated {
                        open.push(CTNode::new(cons, solu));
                    }
                }
            },
            None => return Some(min_cost.solution),
        }
    }

    None
}



/// agent
struct Agent<'a,'b, N, C, FN:'a, FH:'b> {
    start:      N,
    // goal:       N,
    neighbours: &'a FN,
    heuristic:  &'b FH,
    open_list:  BinaryHeap<SmallestCostHolder<C, (C, usize)>>,
    close_list: IndexMap<N, (usize, C)>,
}

impl<'a,'b, N, C, FN, IN, FH> Agent<'a,'b, N, C, FN, FH>
where
    N: Clone + Hash + Eq,
    C: Ord + Zero + Copy,
    FN: Fn(&N) -> IN + 'a,
    IN: IntoIterator<Item = (N, C)>,
    FH: Fn(&N) -> C + 'b,
{
    fn new(
        start: &N, goal: &N, neighbours: &'a FN, heuristic: &'b FH
    ) -> Self {
        let mut a = Agent {
            start:      start.clone(),
            // goal:       goal.clone(),
            neighbours: neighbours,
            heuristic:  heuristic,
            open_list:  BinaryHeap::new(),
            close_list: IndexMap::new(),
        };

        a.open_list.push(SmallestCostHolder {
            estimated_cost: heuristic(goal),
            cost: Zero::zero(),
            payload: (Zero::zero(), 0),
        });
        a.close_list.insert(goal.clone(), (usize::MAX, Zero::zero()));

        a
    }

    fn find_solution(&mut self) -> Option<Vec<N>> {
        let curr = self.start.clone();
        let start = self.rr_astar(&curr);

        if start != 0 {
            let path = itertools::unfold(start, |i| {
                self.close_list.get_index(*i).map(|(node, value)| {
                    let (j, _) = value;
                    *i = *j;
                    node.clone()
                })
            }).collect::<Vec<N>>();
            return Some(path);
        }

        None
    }

    fn update_solution(
        &mut self,
        constraints: &Constraints<N>,
        path: &mut Vec<N>
    ) -> bool {
        true
    }

    /// get N abstract distance
    fn abstract_dist_get(&mut self, curr: &N) -> Option<C> {
        if let Some(&(_, c)) = self.close_list.get(curr) {
            return Some(c);
        }

        if self.rr_astar(curr) != 0 {
            return self.abstract_dist_get(curr);
        }

        None
    }

    /// Reverse Resumable A*
    fn rr_astar(&mut self, curr: &N) -> usize {
        while let Some(SmallestCostHolder {
            payload: (cost, i), ..
        }) = self.open_list.pop()
        {
            let neighbours = {
                let (node, &(_, c)) = self.close_list.get_index(i).unwrap();
                if node == curr {
                    return i;
                }
                // We may have inserted a node several time into the binary heap if we found
                // a better way to access it. Ensure that we are currently dealing with the
                // best path and discard the others.
                if cost > c {
                    continue;
                }
                (self.neighbours)(node)
            };
            for (neighbour, move_cost) in neighbours {
                let new_cost = cost + move_cost;
                let h; // heuristic(&neighbour)
                let n; // index for neighbour
                match self.close_list.entry(neighbour) {
                    Vacant(e) => {
                        h = (self.heuristic)(e.key());
                        n = e.index();
                        e.insert((i, new_cost));
                    }
                    Occupied(mut e) => if e.get().1 > new_cost {
                        h = (self.heuristic)(e.key());
                        n = e.index();
                        e.insert((i, new_cost));
                    } else {
                        continue;
                    },
                }

                self.open_list.push(SmallestCostHolder {
                    estimated_cost: new_cost + h,
                    cost,
                    payload: (new_cost, n),
                });
            } 
        }

        0
    }
}


/// SmallestCostHolder for BinaryHeap
struct SmallestCostHolder<K, P> {
    estimated_cost: K,
    cost: K,
    payload: P,
}

impl<K: PartialEq, P> PartialEq for SmallestCostHolder<K, P> {
    fn eq(&self, other: &SmallestCostHolder<K, P>) -> bool {
        self.estimated_cost.eq(&other.estimated_cost) && self.cost.eq(&other.cost)
    }
}

impl<K: PartialEq, P> Eq for SmallestCostHolder<K, P> {}

impl<K: Ord, P> PartialOrd for SmallestCostHolder<K, P> {
    fn partial_cmp(&self, other: &SmallestCostHolder<K, P>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<K: Ord, P> Ord for SmallestCostHolder<K, P> {
    fn cmp(&self, other: &SmallestCostHolder<K, P>) -> Ordering {
        match other.estimated_cost.cmp(&self.estimated_cost) {
            Ordering::Equal => self.cost.cmp(&other.cost),
            s => s,
        }
    }
}