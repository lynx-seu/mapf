
# Multi-Agent Path Finding
多个Agent(游戏人物, AGV, ROBOT等)同时运动的路径规划问题.  
单个Agent静态环境的路径规划已经很成熟了, 存在很多方法(A*, dijkstra, D*, LFA*等),
但对于多个Agent由于需要解决运动过程中可能引起的相撞冲突问题, 仍不能很有效的解决.

## 可能存在冲突
假设所有Agent速度相等, 则存在两种冲突:
1. 同时到达十字路口
2. 在某条路相遇

## 例子
假设有如下地图， 其中ABCDO为顶点, AO,BO,CO,DO为双向边
```
        D
        |
        |
A-------O-------C
        |
        |
        B
```
### 1. 十字路口冲突
`Agent1` 的路径 {A, O, D}  
`Agent2` 的路径 {B, O, C}  
最终于O相遇

### 2. 同一条路相向行驶
`Agent1` 的路径 {A, O, C}  
`Agent2` 的路径 {C, O, A}  
最终于O相遇



## 针对多运动目标的动态路径规划

### CBS (Conflict-Based Search For Optimal Multi-Agent Path Finding)
已知此模型解决不了如下场景:
1. 假设 A1 存在唯一路径 P1, A2 存在唯一路径 P2, A1与A2相向而行, 且 P1 包含 P2.
解决方案，　A1/A2找避站点，　由于A*算法无法回头(所有评估过点加入了close list),
可以分段使用A*规划．

### SIPP (Safe Interval Path Planning for Dynamic Environments)

### MAPF with Kinematic Constraints